===========================================
README for Taverna based configurable model
===========================================

1. Introdution
==============
This configurable model aims to provide a wrapping mechanism for the executable script, such as Shell, Matlab, COPASI, etc. 
It is built based on Taverna workflow management system. By using this workflow template created based on Taverna, it provides a generic interface to execute scripts in the target server with corresponding inputs and outputs. The following text will introduce the usages of it.

2. I/O files
============

2.1. Inputs
~~~~~~~~~~~

The Inputs include:

a. **command string**
    The example is: *<bash> <script> <input1> <input2> <output1>*. All the text in angle brackets are place holders that will be replaced by the information provided by configuration file and IO list, which will be introduced in following content. Surely, you can put fixed text in the command string as well, such as the flags. The following compiling process will simply ignore the text that is not in the angle brackets.

b. **configuration file**
    It is used to define the executable location and initializing commands for the execution. Each server may have their own configuration file. The example is shown as below:
    ::
   
        {    
            "executables" : {
                "bash" : "/bin/bash",
                "matlab" : "<the path to the matlab executable>",
                ...
            },
            "initialization" : ["touch test", "<any other command you need to run before the call of script>", ...]
        }

c. **IO list**
    It is used to define the relative paths the I/O files in the folder (zipped), which contains the script and I/O files, to let the configurable model replace the right placeholder with right path. The example is shown as followed:
    ::

        {
            "script" : "test.sh",
            "inputList" : {
                "input1" : "testinput1",
                "input2" : "testinput2"
            },
            "outputList" : {
                "output1" : "testoutput1"
            },
            "outputFolder" : ""
        } 
    
    If you do not need to zip certain folder as a whole for output, you can leave the outputFolder as blank. In this case, only the files appeared in outputList will be zipped and transferred back. Or everything in the folder path you write in the outputFolder field will be zipped and transferred back.

d. **sandbox ID**
    This is only a string to separate the identification of each model instance. All the model contents, script, files will be put in the sub-folder with this string as the name in the designated sandbox folder, which is /tmp/sandbox/ as default. If you put 1 as the sandbox ID, the files will all be placed in sandbox /tmp/sandbox/1/.

e. **sandbox ZIP**
    This is where you put all the related files (script, I/O or any other dependencies) in the zip.

2.2. Outputs
~~~~~~~~~~~~

a. **output ZIP**

    All the output files are contained in this zip file.

b. **STDOUT** and **STDERR**

    They are the standard output stream from the execution.

3. Other Configuration Options
==============================

a. sandbox location:
    If you want to change the sandbox location, two places need to be changed. The line 18 of beanshell code and line 1 of tool service code:
	::
	
	    line 18: static String sandbox_location = "/tmp/sandbox";

	    line 1: sandbox_location="/tmp/sandbox/"

4. Examples
===========

a. CopasiSE:
    In the examples folder, there is a zip file contains the CoapsiSE based example. The example from Copasi "brusselator-model" is used. It contains all the necessary files for the execution. The copasi_input.xml is the input for taverna workflow, which contains the command-line template. The brusselator-model.xml.zip contains the SBML file as input. There are also configure.json and IO_List.json. **You need to specify the absolute path of your CopasiSE in the configure.json.**